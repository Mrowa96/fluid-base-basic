<?php
    namespace app\commands;
    
    use Yii;
    use yii\console\Controller;
    
    class RbacController extends Controller
    {
        public function actionInit()
        {
            $auth = Yii::$app->authManager;
    
            $permission = $auth->createPermission('save settings');
            $permission->description = 'Save settings';
            $auth->add($permission);
    
            $role = $auth->getRole("admin");
            $auth->addChild($role, $permission);
        }
    }