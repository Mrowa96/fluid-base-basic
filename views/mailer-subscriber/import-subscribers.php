<?php
    use app\utilities\ActiveForm;
    use yii\helpers\Html;

    $this->title = Yii::t('mailer_subscriber', 'Import Subscribers');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailer'), 'url' => ['mailer/index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mailer-subscriber-import box box-form">
    <div class="box-content">
        <div class="mailer-subscriber-import-form">
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 csvFile">
                        <span class="text">
                            <?= Yii::t("system", "Import from CSV file"); ?>
                        </span>
<pre>
    email | name | join_date
    test@test.com | Jan | 2016-01-01 00:00:00
    test1@test.com | Andrzej | 2016-01-01 00:00:00
</pre>
                        <?= $form->field($model, 'csvFile')->fileInput()->label(false); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('system', 'Import'), ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
