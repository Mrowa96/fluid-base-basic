<?php
    use app\assets\AppAsset;
use app\utilities\Message;
use yii\helpers\Html;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <?= Html::csrfMetaTags() ?>

            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body>
            <?php $this->beginBody() ?>

            <div class="wrapper auth">
                <header class="header">
                    <a href="/">
                        <i class="fa fa-home"></i>
                    </a>
                    <span class="title"><?= $this->title; ?></span>
                </header>

                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                            <?php
                                if(isset($content)){
                                    echo $content;
                                }
                            ?>
                        </div>
                    </div>
                </div>

                <?= Message::widget(['view' => $this]); ?>
                <?= $this->render("@app/views/partials/footer"); ?>
            </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>


