<?php
    use app\assets\AppAsset;
    use app\models\Settings\System;
    use yii\helpers\Html;
    use yii\widgets\Breadcrumbs;
    use app\utilities\Message;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body class="hold-transition <?= $this->params['theme']; ?> top-navigation">
        <?php $this->beginBody() ?>

        <div class="wrapper" >
            <?= $this->render("@app/views/partials/header_with_nav"); ?>
            <?= $this->render("@app/views/partials/loadScreen"); ?>

            <div class="content-wrapper container-fluid">
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-lg-offset-1 col-sm-12 col-xs-12">
                        <section class="content-header">
                            <?= Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'homeLink' => [
                                    'label' => \app\models\Settings::getOne('site_title', System::MODULE)
                                ]
                            ]) ?>
                        </section>

                        <div class="clearFloat"></div>

                        <section class="content">
                            <?php
                            if(isset($content)){
                                echo $content;
                            }
                            ?>
                        </section>
                    </div>
                </div>
            </div>
            <?= Message::widget(['view' => $this]); ?>
            <?= $this->render("@app/views/partials/footer"); ?>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>


