<?php
    use app\assets\AppAsset;
use app\utilities\Message;
use yii\helpers\Html;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <?= Html::csrfMetaTags() ?>

            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="error">
            <?php $this->beginBody(); ?>

            <?php
                if(isset($content)){
                    echo $content;
                }
            ?>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>


