<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailer'), 'url' => ['mailer/index']];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="mailer-message-part-update box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
