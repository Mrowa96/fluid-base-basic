<?php
    use app\models\Language;
    use yii\helpers\Html;
    use app\utilities\ActiveForm;
    $this->registerJsFile("/scripts/_tinymce/tinymce.min.js");
?>

<div class="mailer-message-part-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.loadTinyMCE(document.querySelector("#mailermessagepart-content"));
            });
        </script>
    <?php ActiveForm::end(); ?>
</div>
