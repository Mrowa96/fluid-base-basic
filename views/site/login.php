<?php
    /**
     * @var \app\forms\Login $model
     */
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $this->title = Yii::t("auth", 'Login page');
?>
<div class="site-login">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="form-group">
            <div class="form-message form-message-primary">
                <p><?= Yii::t("auth", "Test accounts"); ?></p>
                <p>Login: admin, <?= Yii::t("auth", "Password");?>: q@Werty</p>
                <p>Login: worker, <?= Yii::t("auth", "Password");?>: worker</p>
            </div>
        </div>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t("auth", "Login"), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <?= Html::a(Yii::t("auth", "Reset password"), ['site/request-password-reset'],['class' => 'btn btn-warning', 'name' => 'login-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
