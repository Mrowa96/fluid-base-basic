<?php
    use yii\helpers\Url;

    $this->title = Yii::t("navigation", "Home");
?>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-map"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba X</span>
                <span class="info-box-number"><?= 23; ?></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba Y</span>
                <span class="info-box-number"><?= 4; ?></span>
            </div>
        </div>
    </div>

    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-object-group"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba Z</span>
                <span class="info-box-number"><?= 1; ?></span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-group"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba użytkowników</span>
                <span class="info-box-number"><?= count($users); ?></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statystyki X</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong>Liczba X w roku 2016</strong>
                        </p>
                        <div class="chart">
                            <canvas id="salesChart" style="height: 180px;"></canvas>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t("system", "Users"); ?></h3>
                <div class="box-tools pull-right">
                    <span class="label label-danger"><?= count($users); ?> <?= (count($users) > 1) ? Yii::t("system", "Users (Plural)") : Yii::t("system", "Users (Singular)"); ?></span>
                </div>
            </div>
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    <?php if (isset($users) && !empty($users)): ?>
                        <?php foreach ($users AS $user): ?>
                            <li>
                                <a class="users-list-avatar" href="<?= Url::toRoute(["/user/view", 'id' => $user->id]); ?>">
                                    <img src="<?= $user->avatar; ?>" alt="User Avatar">
                                </a>

                                <a class="users-list-name" href="<?= Url::toRoute(["/user/view", 'id' => $user->id]); ?>">
                                    <?= $user->username; ?>
                                </a>
                                <span class="users-list-date"><?= $user->created_at; ?></span>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="box-footer text-center">
                <a href="<?= Url::toRoute("/user"); ?>" class="uppercase"><?= Yii::t("system", "Show all"); ?></a>
            </div>
        </div>
    </div>
</div>