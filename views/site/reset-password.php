<?php
    /**
     * @var $this yii\web\View
     * @var $form yii\bootstrap\ActiveForm
     * @var $model \app\forms\ResetPassword
     */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $this->title = Yii::t("auth", "Reset password");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <p><?= Yii::t("auth", "Please choose your new password");?>:</p>

    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password_repeat')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t("system", "Save"), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
