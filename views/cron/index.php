<?php
    /** @var array $actions */
    use yii\helpers\Url;

    $this->title = Yii::t("auth", 'Fluid Base Basic Cron Web Service');
?>

<div class="cron-index">
    <ul id="cronActions">
        <?php if(!empty($actions)): ?>
            <h4>Actions</h4>
            <?php foreach($actions AS $action): ?>
                <li>
                    <span><?= $action; ?></span>
                    <a href="<?= Url::toRoute(['cron/' . $action]); ?>">Link</a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
</div>

