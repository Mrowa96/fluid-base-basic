<?php
    /** @var \app\models\User $user */
    /** @var \app\utilities\ClassicMenu $menu */

    use yii\helpers\Url;

    $user = Yii::$app->user->identity;
    $menu = (isset($this->params['menu'])) ? $this->params['menu'] : null;
?>
<header class="main-header">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="header-menu menu-top">
            <ul class="navbar-nav visible-xs">
                <li>
                    <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                        <span><?= Yii::t("system", "Navigation");?></span>
                    </button>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-custom-menu" id="navbar">
                <?php if($menu): ?>
                    <?= $menu->render(); ?>
                <?php endif; ?>
            </div>

            <div class="dropdown user user-menu hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?= $user->avatar; ?>" class="user-image" alt="User Image">
                        <span class="hidden-md hidden-sm hidden-xs">
                            <?= $user->username;?>
                        </span>
                </a>
                <ul class="dropdown-menu animated-dropdown-menu">
                    <li class="user-header">
                        <img src="<?= $user->avatar; ?>" class="img-circle" alt="User Image">
                        <p>
                            <?= $user->username; ?>
                            <small><?= Yii::t("system", "Joined"); ?> <?= $user->created_at; ?></small>
                        </p>
                    </li>

                    <li class="user-footer">
                        <div class="text-center">
                            <a href="<?= Url::toRoute("/logout"); ?>" class="btn btn-default btn-flat"><?= Yii::t("system", "Logout"); ?></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>