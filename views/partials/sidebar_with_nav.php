<?php
    /**
     * @var \app\utilities\ClassicMenu $menu
     * @var \app\models\User $user
     */

    use yii\helpers\Url;

    $user = Yii::$app->user->identity;
    $menu = (isset($this->params['menu'])) ? $this->params['menu'] : null;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <a href="<?= Url::toRoute(["user/view", 'id' => $user->id]); ?>">
                    <img src="<?= $user->avatar;?>" class="img-circle" alt="User Image">
                </a>
            </div>
            <div class="pull-left info">
                <p>
                    <a href="<?= Url::toRoute(["user/view", 'id' => $user->id]); ?>">
                        <?= $user->username;?>
                    </a>
                </p>
                <a href="mailto:<?= $user->email;?>">
                    <i class="fa fa-envelope text-success"></i>
                    <?= $user->email; ?>
                </a>
            </div>
        </div>

        <?php if($menu): ?>
            <div class="menu">
                <span class="header"><?= Yii::t("system", "Navigation"); ?></span>
                <?= $menu->render(); ?>
            </div>
        <?php endif; ?>
    </section>
</aside>