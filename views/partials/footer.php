<?php
use app\models\Settings;
use app\models\Settings\System;

/** @var Settings $footerText */
$footerText = Settings::getOne('footer_text', System::MODULE, false);
?>

<footer class="main-footer">
    <span><?= ($footerText->value) ? $footerText->value : $footerText->default; ?></span>
</footer>