<?php
    use app\models\Language;
    use yii\helpers\Html;
    use app\utilities\ActiveForm;

    $this->registerJsFile("/scripts/_tinymce/tinymce.min.js");
?>

<div class="mailer-message-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php if($model->isNewRecord): ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?php else: ?>
            <?= $form->field($model, 'name')->textInput(['readonly' => true]) ?>
        <?php endif; ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?php if(!empty($model->tags)): ?>
            <div class="form-message form-message-primary">
                <p class="text-big">
                    <?= Yii::t("mailer_message", "In this message, you can use tags, which will be replaced by content during send.");?>
                </p>
                <ul>
                    <?php foreach($model->tags AS $tag): ?>
                        <li>{<?= $tag->name; ?>} - <?= Yii::t("mailer_message_tag", $tag->description); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'language')->dropDownList(Language::prepareForForm(), [
            'prompt' => Yii::t("settings", "Choose language")
        ]) ?>
        <div class="form-group form-message form-message-info">
            <span><?= Yii::t("mailer_message", "Remember to check if default header and default footer exists in selected language.");?></span>
        </div>
        <?= $form->field($model, 'add_header')->checkbox() ?>
        <?= $form->field($model, 'add_footer')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.loadTinyMCE(document.querySelector("#mailermessage-content"));
            });
        </script>
    <?php ActiveForm::end(); ?>
</div>
