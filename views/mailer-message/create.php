<?php
    $this->title = Yii::t('mailer_message', 'Create Message');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailer'), 'url' => ['mailer/index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mailer-message-create box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
