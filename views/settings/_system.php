<?php
    use app\models\Settings;
    use app\models\Settings\System;

    $this->registerJsFile("/scripts/_tinymce/tinymce.min.js");
?>
<div class="system">
    <?= $form->field($model, 'site_title'); ?>
    <?= $form->field($model, 'theme')->dropDownList(
        $themes,
        ['prompt'=>'Wybierz motyw']
    ); ?>
    <?= $form->field($model, 'language_level')->dropDownList(System::getLanguageLevel(), [
        'prompt' => Yii::t("settings", "Choose language level")
    ]) ?>
    <?php if(Settings::getOne('language_level', System::MODULE) === "global"): ?>
        <?= $form->field($model, 'language')->dropDownList($languages, [
            'prompt' => Yii::t("settings", "Choose language")
        ]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'navigation_position')->dropDownList(System::getNavigationPositions(), [
        'prompt' => Yii::t("settings", "Choose navigation position")
    ]) ?>
    <?= $form->field($model, 'footer_text'); ?>

    <script>
        window.addEventListener("load", function(){
            FI.Lib.loadTinyMCE(document.querySelector("#system-footer_text"));
        })
    </script>

    <?= $form->field($model, 'file_manager')->checkbox(); ?>
</div>