<?php
    use app\utilities\ActiveForm;
    use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['id' => 'language-form']); ?>
    <?= $form->field($model, 'name') ?>
    <div class="form-group">
        <div class="form-message form-message-primary">
            <?= Yii::t("language", "For consistency, all locale IDs should be canonicalized to the format of eg. en_US or pl_PL"); ?>
        </div>
    </div>
    <?= $form->field($model, 'symbol') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("system", "Create") : Yii::t("system", "Update"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
