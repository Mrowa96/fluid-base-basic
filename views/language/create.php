<?php
    $this->title = Yii::t("language", "Add language");;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Languages"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-create box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
