<?php
    use fluid\elements\TabbedView;
    use app\utilities\GridView;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $this->title = Yii::t('navigation', 'Mailer');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mailer-index box">
    <div class="box-content">
        <?php (new TabbedView())->addControl(Yii::t("mailer", "Operations"), "send")
            ->addControl(Yii::t("mailer", "Messages"), "messages")
            ->addControl(Yii::t("mailer", "Message parts"), "messagesParts")
            ->addControl(Yii::t("mailer", "Subscribers"), "subscribers")
            ->addControl(Html::a("+ " . Yii::t("mailer","Send message"), ['create'], ['class' => 'controlBtn controlBtn-success']))
            ->addControl(Html::a("+ " . Yii::t("mailer","Create message"), ['mailer-message/create'], ['class' => 'controlBtn controlBtn-info']))
            ->addControl(Html::a("+ " . Yii::t("mailer","Import subscribers"), ['mailer-subscriber/import'], ['class' => 'controlBtn controlBtn-warning']))
            ->addControl(Html::a("+ " . Yii::t("mailer","Export subscribers"), ['mailer-subscriber/export'], ['class' => 'controlBtn controlBtn-error']))
            ->addSection(GridView::widget([
                'dataProvider' => $newsletters,
                'columns' => [
                    'message.title',
                    'status_text',
                    'send_date'
                ],
                'buttons' => [
                    'cancel' => function($url, $model){
                        if($model->waiting_for_send){
                            return '<a href="'.Url::toRoute(['mailer/cancel', 'id' => $model->id]).'" title="'.Yii::t("mailer","Cancel").'" class="action"></a>';
                        }
                    },
                ],
                'layout'=>"{items}\n{pager}"
            ]), "send")
            ->addSection(GridView::widget([
                'dataProvider' => $messagesProvider,
                'columns' => [
                    'title',
                    'language',
                    'system_text'
                ],
                'buttons' => [
                    'update' => function($url, $model){
                        return '<a href="'.Url::toRoute(['mailer-message/update', 'id' => $model->id]).'" title="'.Yii::t("system","Update").'" class="action"></a>';
                    },
                    'delete' => function($url, $model){
                        if($model->system == 0){
                            return '<a href="'.Url::toRoute(['mailer-message/delete', 'id' => $model->id]).'" title="'.Yii::t("system","Delete").'" class="action"></a>';
                        }
                    },
                ],
                'rowOptions'=>function($model){
                    if($model->system == 1){
                        return ['class' => 'system-message'];
                    }
                },
                'layout'=>"{items}\n{pager}"
            ]), "messages")
            ->addSection(GridView::widget([
                'dataProvider' => $messagesPartsProvider,
                'columns' => [
                    'title',
                    'language',
                ],
                'buttons' => [
                    'update' => function($url, $model){
                        return '<a href="'.Url::toRoute(['mailer-message-part/update', 'id' => $model->id]).'" title="'.Yii::t("system","Update").'" class="action"></a>';
                    },
                ],
                'layout'=>"{items}\n{pager}"
            ]), "messagesParts")
            ->addSection(GridView::widget([
                'dataProvider' => $subscribersProvider,
                'columns' => [
                    'name',
                    'email',
                    'join_date',
                ],
                'buttons' => [
                    'delete' => function($url, $model){
                        return '<a href="'.Url::toRoute(['mailer-subscriber/delete', 'id' => $model->id]).'" title="'.Yii::t("system","Delete").'" class="action"></a>';
                    },
                ],
                'layout'=>"{items}\n{pager}"
            ]), "subscribers")
            ->setDefault("send")
            ->render();
        ?>
    </div>
</div>
