<?php
    $this->title = Yii::t('mailer', 'Send message');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailer'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mailer-create box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'messages' => $messages,
        ]) ?>
    </div>
</div>
