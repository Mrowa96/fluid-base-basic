<?php
    use kartik\datetime\DateTimePicker;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="mailer-form">
    <?php $form = ActiveForm::begin(['id' => 'mailerForm']); ?>
        <?= $form->field($model, 'mailer_message_id')->dropDownList(
            $messages,
            ['prompt' => Yii::t("mailer", "Choose a message")]
        ) ?>
        <?= $form->field($model, 'send_date')->widget(DateTimePicker::className(),[
            'name' => 'send_date',
            'type' => DateTimePicker::TYPE_INPUT,
            'options' => ['id' => 'send_date' . $model->id],
            'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-MM-dd H:i',
                'todayHighlight' => true,
                'startDate' => (new \DateTime())->format("Y-m-d H:i:s")
            ]
        ]); ?>
        <?= $form->field($model, 'send_to')->input("hidden", ['id' => 'hiddenInput'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('mailer', 'To workers'), ['class' => 'btn btn-success', 'id' => 'toWorkersBtn', 'data-send-to' => 'workers']) ?>
            <?= Html::submitButton(Yii::t('mailer', 'To subscribers'), ['class' => 'btn btn-primary', 'id' => 'toSubscribersBtn', 'data-send-to' => 'subscribers']) ?>
        </div>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.handleMailerSend(document.querySelector("#mailerForm"));
            })
        </script>
    <?php ActiveForm::end(); ?>
</div>
