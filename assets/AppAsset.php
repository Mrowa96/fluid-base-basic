<?php
    namespace app\assets;

    use yii\web\AssetBundle;
    use \yii\web\View;

    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            'styles/layout.min.css',
            'styles/skins.min.css',
            'styles/_font-awesome/font-awesome.min.css',
            'styles/_fluid-elements/app.min.css',
            'styles/_project/app.min.css',
        ];
        public $js = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
            'scripts/_yii/cookie.min.js',
            'scripts/_fluid-elements/fluid-core.min.js',
            'scripts/_fluid-elements/fluid-instance.min.js',
            'scripts/_slimScroll/jquery.slimscroll.min.js',
            'scripts/_chartjs/Chart.min.js',
            'scripts/admin.min.js',
        ];
        public $jsOptions = ['position' => View::POS_HEAD];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
