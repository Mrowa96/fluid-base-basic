Fluid Base Basic
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      extras/             contains extra data like example vhost or db dump        
      forms/              contains models which are forms
      models/             contains model classes
      runtime/            contains files generated during runtime
      searches/           contains models which are searches
      vendor/             contains dependent 3rd-party packages
      utilities/          contains utility classes
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.5.0.


INSTALLATION
------------
If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following commands:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install

~~~


CONFIGURATION
-------------
#### Vhost

Create your vhost file, it can be based on example vhost, which you can find in extras directory.
Remember that application has own .htaccess file.

#### Database

Set up host, database name, login, password and a prefix in config/db.php (if not exists, just copy db-example.php and rename it), for example

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=fluid-base-basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
    'tablePrefix' => 'fbb_'
];
```

Next, import database data from extras/example.sql

#### Apache configuration file

If you are using ssl on your server rename .htaccess.ssl to .htaccess

ACCESS
------
#### Admin account
Login: admin
Password: q@Werty

#### Worker account
Login: worker
Password: worker