<?php
    namespace app\controllers;

    use Yii;
    use app\models\Settings;
    use app\models\Settings\Information;
    use app\models\Settings\Sms;
    use app\models\Settings\Smtp;
    use app\models\Settings\System;
    use app\models\Language;

    class SettingsController extends BaseController{

        public function actionIndex()
        {
            if(Yii::$app->user->can("display settings")){
                $model = new Settings();;
                $data = [
                    'model' => $model,
                    'themes' => Settings::getThemes(),
                    'languages' => Language::prepareForForm(),
                    'information' => (new Information())->prepareForForm(),
                    'smtp' => (new Smtp())->prepareForForm(),
                    'system' => (new System())->prepareForForm(),
                    'sms' => (new Sms())->prepareForForm()
                ];

                if(Yii::$app->request->isPost){
                    if(Yii::$app->user->can("save settings")){
                        if ($model->loadData(Yii::$app->request->post()) && $model->saveData()) {
                            return $this->redirectWithMessage(['index'], $this->getMessage("settingsSaved"));
                        }

                        return $this->redirectWithMessage(['index'], $this->getMessage("unknownError"), "error");
                    }
                    else{
                        return $this->redirectWithMessage(['index'], $this->getMessage("noPermissions"), "warning");
                    }
                }
                else {
                    return $this->render('index', $data);
                }
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noAccess"), "warning");
            }
        }
    }
