<?php
    namespace app\controllers;

    use Yii;
    use yii\filters\AccessControl;
    use yii\helpers\ArrayHelper;
    use yii\web\Controller;
    use app\models\Menu;
    use app\models\Settings;
    use app\utilities\ClassicMenu;
    use app\models\Settings\System;
    use app\utilities\Language;

    class BaseController extends Controller{

        private $messages = [];

        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['login', 'request-password-reset', 'reset-password', "error"],
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => true,
                            'roles' => ['admin', 'worker'],
                        ],
                    ],
                ],
            ];
        }
        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function beforeAction($action)
        {
            $language = new Language();
            $language->manage();

            $session = Yii::$app->session;
            if($session->has("messageCounter")){
                $counter = $session->get("messageCounter");

                if($counter < 1){
                    if($session->has("message")){
                        $session->remove("message");
                    }
                    if($session->has("type")){
                        $session->remove("type");
                    }
                }
                else{
                    $session->set("messageCounter", 0);
                }
            }

            switch(Settings::getOne('navigation_position', System::MODULE)){
                case "top":
                    $this->layout = "main_top_nav";
                    $menu = new ClassicMenu(Menu::fetchForMenu(), [
                        'class' => 'header-menu nav navbar-nav',
                        'position' => 'top'
                    ]);
                    break;
                case "left":
                default:
                    $menu = new ClassicMenu(Menu::fetchForMenu());
                    $this->layout = "main_left_nav";
                    break;
            }

            $this->view->title = Settings::getOne('site_title', System::MODULE);
            $this->view->params['theme'] = Settings::getOne('theme', System::MODULE);
            $this->view->params['menu'] = $menu;

            $this->messages = [
                'noView' => "Object cannot be viewed.",
                'noCreate' => "Object cannot be created.",
                'noUpdate' => "Object cannot be updated.",
                'noDelete' => "Object cannot be deleted.",
                'okCreate' => "Object was created successfully.",
                'okUpdate' => "Object was updated successfully.",
                'okDelete' => "Object was deleted successfully.",
                'noPermission' => "Error occured, you don't have administrator permission.",
                'noAccess' => "You do not have correct permissions to access that site.",
                'unknownError' => "An unexpected error occurred, please contact with administrator.",
                'settingsSaved' => "Settings have been successfully saved.",
            ];

            return parent::beforeAction($action);
        }

        public function redirectWithMessage(array $url, $message, $type = "success", $statusCode = 302)
        {
            Yii::$app->session->set("message", $message);
            Yii::$app->session->set("type", $type);
            Yii::$app->session->set("messageCounter", 1);

            return parent::redirect($url, $statusCode);
        }
        public function renderWithMessage($view, $message, $params = [], $type = "success")
        {
            $this->view->params['message'] = $message;
            $this->view->params['type'] = $type;

            return parent::render($view, $params);
        }

        public function getMessage($index)
        {
            if(isset($this->messages[$index])){
                return Yii::t("system", $this->messages[$index]);
            }
            else{
                return Yii::t("system", "Message not found");
            }
        }

        public function getRoles()
        {
            $roles = Yii::$app->authManager->getRoles();

            return ArrayHelper::map($roles, 'name', 'description');
        }
    }
