<?php
    namespace app\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use app\models\Language;

    class LanguageController extends BaseController
    {
        public function actionIndex()
        {
            if(Yii::$app->user->can("display languages")){
                $dataProvider = new ActiveDataProvider([
                    'query' => Language::find()
                ]);

                return $this->render('index', [
                    'dataProvider' => $dataProvider
                ]);
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noAccess"), "warning");
            }
        }

        public function actionCreate()
        {
            if(Yii::$app->user->can("save language")){
                /** @var Language $model */
                $model = new Language();

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirectWithMessage(['index'], $this->getMessage('okCreate'));
                }

                return $this->render('create', [
                    'model' => $model
                ]);
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noPermissions"), "warning");
            }
        }
        public function actionUpdate($id)
        {
            if(Yii::$app->user->can("save language")){
                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirectWithMessage(['index'], $this->getMessage('okUpdate'));
                }

                return $this->render('update', [
                    'model' => $model
                ]);
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noPermissions"), "warning");
            }
        }
        public function actionManage($id)
        {
            if(Yii::$app->user->can("save language")){
                if(Yii::$app->request->isPost && Language::saveMessages()) {
                    return $this->redirect(['index']);
                }
                else{
                    return $this->render('manage', [
                        'data' => Language::prepareForManage($id),
                    ]);
                }
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noPermissions"), "warning");
            }
        }
        public function actionDelete($id)
        {
            if(Yii::$app->user->can("save language")){
                $model = $this->findModel($id);

                if($model->delete()){
                    return $this->redirectWithMessage(['index'], $this->getMessage('okDelete'));
                }
                else{
                    return $this->redirectWithMessage(['index'], $this->getMessage('noDelete'), "error");
                }
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage("noPermissions"), "warning");
            }
        }
        protected function findModel($id){
            /** @var Language $model */
            if (($model = Language::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }