<?php
    namespace app\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use app\models\Mailer;
    use app\models\MailerMessage;
    use app\models\MailerMessagePart;
    use app\models\MailerSubscriber;
    use yii\web\NotFoundHttpException;

    class MailerController extends BaseController
    {
        public function actionIndex()
        {
            $messagesProvider = new ActiveDataProvider([
                'query' => MailerMessage::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $messagesPartsProvider = new ActiveDataProvider([
                'query' => MailerMessagePart::find(),
            ]);
            $subscribersProvider = new ActiveDataProvider([
                'query' => MailerSubscriber::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $newsletters = new ActiveDataProvider([
                'query' => Mailer::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index',[
                'messagesProvider' => $messagesProvider,
                'messagesPartsProvider' => $messagesPartsProvider,
                'subscribersProvider' => $subscribersProvider,
                'newsletters' => $newsletters,
            ]);
        }
        
        public function actionCreate()
        {
            $model = new Mailer();
            $messages = MailerMessage::prepareForForm();

            if($model->load(Yii::$app->request->post())){
                if($model->save() && $model->send()){
                    return $this->redirectWithMessage(['index'], $this->getMessage('okCreate'));
                }
                else{
                    return $this->redirectWithMessage(['index'],  $this->getMessage('noCreate'), "error");
                }
            }
            else{
                return $this->render("create", [
                    'model' => $model,
                    'messages' => $messages,
                ]);
            }
        }

        public function actionCancel($id)
        {
            $model = $this->findModel($id);

            $model->status = Mailer::STATUS_CANCELED;

            if ($model && $model->save()) {
                return $this->redirectWithMessage(['index'], $this->getMessage('okUpdate'));
            }
            else{
                return $this->redirectWithMessage(['index'], $this->getMessage('noUpdate'));
            }
        }

        protected function findModel($id)
        {
            /** @var Mailer $model */
            if (($model = Mailer::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
