<?php
    namespace app\controllers;

    use Yii;
    use yii\web\NotFoundHttpException;
    use app\models\MailerSubscriber;
    use app\forms\MailerSubscriber as MailerSubscriberForm;
    use League\Csv\Writer;

    class MailerSubscriberController extends BaseController
    {
        public function actionExport()
        {
            /** @var Writer $csv */
            $csv = MailerSubscriber::export();
            $csv->output('subscribers.csv');

            exit;
        }

        public function actionImport()
        {
            $model = new MailerSubscriberForm();

            if($model->load(Yii::$app->request->post())){
                if(MailerSubscriber::import($model->getCsvFile())){
                    return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okCreate'));
                }
                else{
                    return $this->redirectWithMessage(['mailer/index'], $this->getMessage('noCreate'), "error");
                }
            }

            return $this->render("import-subscribers", [
                'model' => $model
            ]);
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);

            if($model->delete()){
                return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okDelete'));
            }
            else{
                return $this->redirectWithMessage(['mailer/index'], $this->getMessage('noDelete'), "error");
            }
        }

        protected function findModel($id)
        {
            /** MailerSubscriber $model */
            if (($model = MailerSubscriber::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
