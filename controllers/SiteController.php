<?php
    namespace app\controllers;

    use Yii;
    use app\forms\Login as LoginForm;
    use app\models\User;
    use yii\base\InvalidParamException;
    use yii\web\BadRequestHttpException;
    use app\forms\PasswordResetRequest;
    use app\forms\ResetPassword;
    use app\models\Settings;
    use app\models\Settings\System;
    use app\utilities\Language;

    class SiteController extends BaseController{

        public function beforeAction($action)
        {
            if(parent::beforeAction($action)){
                if($action->id === "error"){
                    $this->layout = "error";
                }

                return true;
            }

            return false;
        }

        public function actionIndex()
        {
        	$this->view->registerJsFile("/scripts/stats.min.js");
        	
            return $this->render('index', [
                'users' => User::find()->where(['status' => User::STATUS_ACTIVE])->all(),
            ]);
        }

        public function actionLogin()
        {
            $this->layout = "auth";

            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->redirect(['index']);
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        public function actionLogout()
        {
            Yii::$app->user->logout();

            $session = Yii::$app->session;
            $session->remove("adminAccess");

            return $this->goHome();
        }
        public function actionRequestPasswordReset()
        {
            $this->layout = "auth";

            $model = new PasswordResetRequest();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try{
                    $model->sendEmail();

                    return $this->redirectWithMessage(['request-password-reset'], Yii::t("system", 'Check your email for further instructions.'));
                }
                catch(\Exception $ex){
                    return $this->redirectWithMessage(['request-password-reset'], $ex->getMessage(), "error");
                }
            }

            return $this->render('request-password-reset', [
                'model' => $model,
            ]);
        }
        public function actionResetPassword($token)
        {
            $this->layout = "auth";

            try {
                $model = new ResetPassword($token);

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    try{
                        $model->resetPassword();
                    }
                    catch(\Exception $ex){
                        return $this->redirectWithMessage(['reset-password'], $ex->getMessage(), "error");
                    }

                    return $this->redirect(['index']);
                }

                return $this->render('reset-password', [
                    'model' => $model,
                ]);
            }
            catch (InvalidParamException $e) {
                throw new BadRequestHttpException($e->getMessage());
            }
        }

        public function actionChangeLang($symbol)
        {
            if(Settings::getOne('language_level', System::MODULE) === "cookie"){
                $language = new Language();
                $language->setCookieLanguage($symbol);
            }

            return $this->goBack();
        }
    }
