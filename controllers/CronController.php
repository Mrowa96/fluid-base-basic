<?php
    namespace app\controllers;

    use Yii;
    use yii\helpers\Json;
    use yii\web\Controller;
    use app\models\CronQueue;
    use app\utilities\Mailer;
    use app\models\MailerMessage;
    use app\models\MailerSubscriber;
    use app\models\MailerData;
    use app\utilities\Utility;
    use app\models\Mailer as MailerModel;
    use app\models\User;
    use ErrorException;

    class CronController extends BaseController
    {
        public function beforeAction($action)
        {
            if(parent::beforeAction($action)){
                $this->layout = "auth";

                return true;
            }

            return false;
        }

        public function actionIndex()
        {
            $class = new \ReflectionClass($this);
            $actions = [];
            $index = 1;

            foreach ($class->getMethods(\ReflectionMethod::IS_PUBLIC) AS $method){
                if ($method->class == $class->getName()){
                    $methodName = Utility::uppercaseToDash(substr($method->name, 6));

                    if($methodName !== "index"){
                        $actions[$index] = $methodName;
                    }
                }
            }

            return $this->render('index', [
                'actions' => $actions
            ]);
        }

        public function actionSendMailer()
        {
            /**
             * @var CronQueue $mailers
             * @var MailerModel $mailerModel
             * @var MailerMessage $message
             * @var MailerData $data
             * @var Mailer $mailer
             */
            $mailers = CronQueue::find()
                ->where(['type' => 'mailer-request'])
                ->andWhere(['status' => 0])
                ->all();

            if(!empty($mailers)){
                /** @var Mailer $mailer */
                $mailer = Yii::$app->mailer;

                foreach($mailers AS $newsletter){
                    $jsonData = Json::decode($newsletter->data, false);

                    if(isset($jsonData->id)){
                        $mailerModel = MailerModel::find()
                            ->where(['id' => $jsonData->id])
                            ->andWhere(['>=', 'send_date', (new \DateTime())->format("Y-m-d H:i:s")])
                            ->one();;
                        $mailerData = MailerData::find()->where(['mailer_id' => $mailerModel->id])->all();
                        $message = MailerMessage::findOne($mailerModel->mailer_message_id);

                        if($message){
                            $message->prepareForSent();

                            try{
                                foreach($mailerData AS $data){
                                    if($data->mailer_user_id){
                                        $subscriber = User::findOne($data->mailer_user_id);
                                    }
                                    else if($data->mailer_subscriber_id){
                                        $subscriber = MailerSubscriber::findOne($data->mailer_subscriber_id);
                                    }

                                    /** @var User | MailerSubscriber $subscriber */
                                    if(isset($subscriber)){

                                        $mailer->compose()
                                            ->setFrom($mailer->getLogin())
                                            ->setTo($subscriber->email)
                                            ->setSubject($message->title)
                                            ->setHtmlBody($message->content)
                                            ->send();
                                    }
                                }

                                $mailerModel->status = MailerModel::STATUS_SEND;
                                $mailerModel->save();
                            }
                            catch(ErrorException $ex){
                                return $this->redirectWithMessage(['index'], $ex->getMessage(), "error");
                            }
                            catch(\Exception $ex){
                                return $this->redirectWithMessage(['index'], $ex->getMessage(), "error");
                            }
                        }
                        else{
                            return $this->redirectWithMessage(['index'], "Cannot find message with ". $mailerModel->mailer_message_id ."id", "error");
                        }
                    }

                    $newsletter->status = 1;
                    $newsletter->save();
                }

                return $this->redirectWithMessage(['index'], "All emails was send successfully");
            }
            else{
                return $this->redirectWithMessage(['index'], "No new emails to send");
            }
        }
    }