<?php
    namespace app\controllers;

    use Yii;
    use yii\web\NotFoundHttpException;
    use app\models\MailerMessage;

    class MailerMessageController extends BaseController
    {
        public function actionCreate()
        {
            $model = new MailerMessage();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okCreate'));
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okUpdate'));
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);

            if($model->system === 0){
                $model->delete();
            }

            return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okDelete'));
        }

        protected function findModel($id)
        {
            /** @var MailerMessage $model */
            if (($model = MailerMessage::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
