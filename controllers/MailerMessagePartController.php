<?php
    namespace app\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use app\models\MailerMessagePart;

    class MailerMessagePartController extends BaseController
    {
        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => MailerMessagePart::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        
        public function actionUpdate($id)
        {
            /** MailMessagePart $model */
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirectWithMessage(['mailer/index'], $this->getMessage('okUpdate'));
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        protected function findModel($id)
        {
            /** MailerMessagePart $model */
            if (($model = MailerMessagePart::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
