(function() {
    var FI = {
        wrapper: document.getElementsByClassName("wrapper")[0],
        started: false,
        elements: {},
        temporary: {},
        dataName: 'FI',
        Lib: {},

        init: function () {
            window.addEventListener("load", function() {
                if (Fluid && FI.started === false) {
                    Fluid.Loader.load(FI);

                    FI.Lib = new FI.Lib();
                    window.FI = FI;

                    console.info("Fluid Instance started.");
                }
            }, false);
        },
        onLoad: function(){
            window.addEventListener("load", function() {});
        }
    };

    FI.Lib = function(){};
    FI.Lib.prototype.selectAvatar = function(conf){
        var avatars, input;

        if(Fluid.Lib.is([conf.avatars, conf.input])){
            avatars = Fluid.Lib.toArray(conf.avatars.getElementsByTagName("img"));
            input = conf.input;

            avatars.forEach(function(avatar){
                avatar.addEventListener("click", function(){
                    avatars.forEach(function(item){
                        item.classList.remove("selected");
                    });

                    input.value = this.src;
                    this.classList.add("selected");
                }, false);
            });
        }
        else{
            console.warn("You should check conf.")
        }
    };
    FI.Lib.prototype.loadTinyMCE = function(elm){
        if(elm) {
            try {
                if (tinymce !== undefined) {
                    var editor = tinymce.init({
                        selector: "#" + elm.id,
                        language: 'pl',
                        height: 300,
                        setup: function(ed) {
                            ed.on('NodeChange', function (e) {
                                elm.innerHTML = ed.getContent();
                            })
                        }
                    });
                }
            }
            catch (e) {
                console.warn("TinyMCE is not defined.")
            }
        }
    };
    FI.Lib.prototype.handleMailerSend = function(elm){
        var toSubscribersBtn, toWorkersBtn, hiddenInput, sendTo;

        if(elm){
            toWorkersBtn = elm.querySelector("#toWorkersBtn");
            toSubscribersBtn = elm.querySelector("#toSubscribersBtn");
            hiddenInput = elm.querySelector("#hiddenInput");

            toWorkersBtn.addEventListener("click", function(){
                sendTo = this.dataset.sendTo;
            });
            toSubscribersBtn.addEventListener("click", function(){
                sendTo = this.dataset.sendTo;
            });

            elm.addEventListener("submit", function(e){
                hiddenInput.value = sendTo;
            });
        }
    };
    FI.Lib.prototype.loadEditor = function(conf){
        var editor, type = "xml";

        if(CodeMirror && Fluid.Lib.is(conf.element)){
            if(conf.type){
                switch(conf.type){
                    case "html":
                        type = "xml";
                        break;
                    case "js":
                        type = "javascript";
                        break;
                    default:
                        type = conf.type;
                        break;
                }
            }

            editor = CodeMirror.fromTextArea(conf.element, {
                lineNumbers: true,
                matchBrackets: true,
                mode: type,
                htmlMode: true
            });

            conf.element.editor = editor;
            editor.on("change", function(){
                conf.element.value = editor.getValue();
            }, false);

            return editor;
        }

        return false;
    };
    FI.Lib.prototype.handleUpdates = function(conf){
        var checkUpdateBtn, updateButton;

        if(conf.element){
            checkUpdateBtn = conf.element.querySelector("#checkUpdate");

            if(checkUpdateBtn){
                checkUpdateBtn.addEventListener("click", function(){
                    Fluid.Http.get({
                        url: "/settings/check-update.html",
                        onResponse: function(response){
                            var res = JSON.parse(response);

                            if(Fluid.Lib.is(res.id)){
                                updateButton = document.createElement("button");
                                updateButton.classList.add("btn", "btn-success");
                                updateButton.innerHTML = "Aktualizuj";
                                conf.element.appendChild(updateButton);

                                updateButton.addEventListener("click", function(){
                                    Fluid.Http.get({
                                        url: "/settings/update-system/" + res.id+ ".html",
                                        onResponse: function(response){
                                            var res = JSON.parse(response);

                                            if(res.message){
                                                Fluid.Notify(res.message, res.type);
                                                updateButton.parentNode.removeChild(updateButton);
                                                location.reload();
                                            }
                                        }
                                    })
                                })
                            }
                            else{
                                new Fluid.Notify(res, "success");
                            }
                        }
                    })
                });
            }
        }
    };
    FI.Lib.prototype.submitAndReturn = function(conf){
        if(conf.form && conf.button){
            conf.button.addEventListener("click", function(e){
                var hiddenInput;
                e = e || window.event;

                console.log("click");
                hiddenInput = document.createElement("input");
                hiddenInput.setAttribute("name", "editAfterSubmit");
                hiddenInput.setAttribute("type", "hidden");
                hiddenInput.value = "1";

                conf.form.appendChild(hiddenInput);
                conf.form.submit();

                e.preventDefault();
            });
        }
    };

    FI.init();
    FI.onLoad();
}());
