<?php
    Yii::setAlias("webDir", Yii::getAlias("@app").'/web');
    Yii::setAlias("dataDir", Yii::getAlias("@webDir").'/data');
    Yii::setAlias("dataUrl", '/data');
    Yii::setAlias("dataSubscribers", Yii::getAlias("@app") . '/runtime/mailer');

    define("DS", "/");