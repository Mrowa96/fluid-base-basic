<?php
    $params = require(__DIR__ . '/params.php');

    $config = [
        'id' => 'basic',
        'language' => 'pl_PL',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'components' => [
            'request' => [
                'cookieValidationKey' => 'S0W1rsWR4MTCgc-cmzrb1OJprwWqsQ65',
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
            ],
            'user' => [
                'identityClass' => 'app\models\User',
                'enableAutoLogin' => true,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'mailer' => [
                'class' => 'app\utilities\Mailer',
            ],
            'smsGate' => [
                'class' => 'app\utilities\SmsGate',
            ],
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => require(__DIR__ . '/db.php'),
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'rules' => [
                    'logout' => 'site/logout',
                    'login' => 'site/login',
                    'stats' => 'site/stats',
                    'get-model-data' => 'site/get-model-data',
                    'save-grid-view-settings' => 'site/save-grid-view-settings'
                ],
                'suffix' => '.html'
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'app\utilities\i18n',
                        'forceTranslation'=>true,
                    ],
                ],
            ],
        ],
        'params' => $params,
    ];

    if (YII_ENV_DEV) {
        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
        ];
    }

    return $config;
