<?php
    return [
        'adminEmail' => 'kontakt@tavoite.pl',
        'user.passwordResetTokenExpire' => 3600,
    ];
