<?php
    namespace app\utilities;
    
    use app\models\Menu;
    use Yii;

    class ClassicMenu
    {
        protected $menuDocument = null;
        protected $ulElement = null;
        protected $options = [];
        protected $controller = null;
        protected $map = [];
        protected $data = [];
        protected $customOptions = [];

        public function __construct(array $data, array $options = []){
            $this->menuDocument = new \DOMDocument('1.0');
            $this->ulElement = $this->menuDocument->createElement("ul");
            $this->controller = Yii::$app->controller->id;
            $this->customOptions = $options;

            $this->assignOptions();
            $this->mapData($data);

            $this->menuDocument->appendChild($this->ulElement);
        }

        public function render()
        {
            $this->parseMap();

            $this->ulElement->setAttribute('class', $this->getOption('class'));

            return $this->menuDocument->saveHTML();
        }

        protected function mapData(array $data)
        {
            /** @var Menu $item */
            if(!empty($data)) {
                foreach ($data AS $item) {
                    if($item->parent_id === 0){
                        $this->map[$item->id]['data'] = $item;
                    }
                    else{
                        //limit to only one children
                        if(!array_key_exists($item->parent_id, $this->map)){
                            $this->map[$item->parent_id] = [];
                        }

                        if(!array_key_exists('children', $this->map[$item->parent_id])){
                            $this->map[$item->parent_id]['children'] = [];
                        }

                        $this->map[$item->parent_id]['children'][$item->id]['data'] = $item;
                    }
                }
            }

        }
        protected function parseMap()
        {
            /**
             * @var Menu $item
             * @var Menu $child
             * @var array $data
             */

            if(!empty($this->map)){
                foreach($this->map AS $data){
                    $item = $data['data'];

                    if($item->admin_access === 1 && (Yii::$app->user->isGuest || !Yii::$app->user->identity->isAdmin)){
                        continue;
                    }

                    $liElement = $this->addLinkElement($item, $this->ulElement);

                    if(array_key_exists('children', $data)){
                        $ulChildElement = $this->menuDocument->createElement("ul");
                        $ulChildElement->setAttribute("class", "treeview-menu");

                        foreach ($data['children'] AS $child){
                            $this->addLinkElement($child['data'], $ulChildElement);
                            $this->data[$child['data']->id] = [
                                'element' => $liElement,
                                'data' => $child['data']
                            ];
                        }

                        $liElement->appendChild($ulChildElement);
                    }

                    $this->data[$item->id] = [
                        'element' => $liElement,
                        'data' => $item
                    ];
                }

                $this->setLinkActive();
            }

            return $this;
        }
        protected function addLinkElement(Menu $item, \DOMElement $ulElement){
            $liElement = $this->menuDocument->createElement("li");
            $linkElement = $this->menuDocument->createElement("a");
            $textElement = $this->menuDocument->createElement("span", Yii::t("navigation", $item->name));

            if($item->icon){
                $iconElement = $this->menuDocument->createElement("i");
                $iconElement->setAttribute("class", $item->icon . ' icon');
                $linkElement->appendChild($iconElement);
            }

            $liClass = $this->getOption('childClass');

            switch($this->getOption('position')){
                case "top":
                    if($item->route == "site"){
                        $textElement->setAttribute('class', 'hidden-sm hidden-lg hidden-md');
                    }
                    else{
                        $textElement->setAttribute('class', 'hidden-sm');
                    }
                    break;
                case "left":
                    //for further use
                    break;
            }

            $linkElement->setAttribute("href", $item->url);
            $liElement->setAttribute("class", $liClass);

            $linkElement->appendChild($textElement);
            $liElement->appendChild($linkElement);
            $ulElement->appendChild($liElement);

            return $liElement;
        }
        protected function setLinkActive()
        {
            /** @var Menu $item */
            foreach($this->data AS $item){
                if($item['data']->route === Yii::$app->controller->id){
                    $item['element']->setAttribute('class', $item['element']->getAttribute('class') . ' ' . 'active');

                    if($item['data']->parent_id){
                        $parent = $this->data[$item['data']->parent_id];
                        $parent['element']->setAttribute('class', $parent['element']->getAttribute('class') . ' ' . 'active');
                    }
                }
            }
        }
        protected function assignOptions()
        {
            $this->options['position'] = $this->checkOption('position', 'left');
            $this->options['class'] = $this->checkOption('class', 'sidebar-menu');
            $this->options['childClass'] = $this->checkOption('childClass', 'treeview link');
        }
        protected function getOption($name)
        {
            if(array_key_exists($name ,$this->options)){
                return $this->options[$name];
            }
            else{
                return null;
            }
        }
        protected function checkOption($name, $default = null)
        {
            if(array_key_exists($name, $this->customOptions) && !empty($this->customOptions[$name])){
                return $this->customOptions[$name];
            }
            else{
                return $default;
            }
        }
    }