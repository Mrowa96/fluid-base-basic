<?php
    namespace app\models;
    
    use fluid\fileManager\File;
    use League\Csv\Reader;
    use League\Csv\Writer;
    use SplTempFileObject;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\db\Query;

    /**
     * This is the model class for table "{{%mailer_subscriber}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $email
     * @property string $join_date
     * @property bool $active
     */
    class MailerSubscriber extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mailer_subscriber}}';
        }
        
        public function rules()
        {
            return [
                [['name', 'email', 'join_date'], 'required'],
                [['join_date'], 'safe'],
                [['active'], 'boolean'],
                [['name', 'email'], 'string', 'max' => 128],
            ];
        }
        
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer_subscriber', 'ID'),
                'name' => Yii::t('mailer_subscriber', 'Name'),
                'email' => Yii::t('mailer_subscriber', 'Email'),
                'join_date' => Yii::t('mailer_subscriber', 'Join Date'),
            ];
        }

        public static function export()
        {
            $csv = Writer::createFromFileObject(new SplTempFileObject());
            $subscribers = MailerSubscriber::find()->all();

            $csv->insertOne(['Nazwa', 'Email', 'Data dołączenia']);

            foreach($subscribers AS $subscriber){
                $csv->insertOne([$subscriber->name, $subscriber->email, $subscriber->join_date]);
            }

            return $csv;
        }
        public static function import(File $file)
        {
            $csv = Reader::createFromPath($file->path);
            $columns = $csv->fetchOne();
            $results = $csv->fetchAssoc($columns);

            try{
                if(in_array('email', $columns) && in_array('join_date', $columns) && in_array('name', $columns)){

                    foreach ($results as $key => $row) {
                        if($key > 0){
                            if(!MailerSubscriber::find()->where(['email' => $row['email']])->one()){
                                $subscriber = new MailerSubscriber();

                                $subscriber->email = $row['email'];
                                $subscriber->name = $row['name'];
                                $subscriber->join_date = $row['join_date'];

                                $subscriber->save();
                            }
                        }
                    }
                }

                $file->remove();

                return true;
            }
            catch(\Exception $ex){
                return false;
            }
        }
        public static function getAll()
        {
            return MailerSubscriber::find()->where(['active' => 1])->all();
        }
    }
