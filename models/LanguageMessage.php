<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%i18n_message}}".
     *
     * @property integer $id
     * @property string $language
     * @property string $translation
     *
     * @property LanguageSource $id0
    */
    class LanguageMessage extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%i18n_message}}';
        }

        public function rules()
        {
            return [
                [['id', 'language'], 'required'],
                [['id'], 'integer'],
                [['translation'], 'string'],
                [['language'], 'string', 'max' => 16],
                [['id'], 'exist', 'skipOnError' => true, 'targetClass' => LanguageSource::className(), 'targetAttribute' => ['id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('language_message', 'ID'),
                'language' => Yii::t('language_message', 'Language'),
                'translation' => Yii::t('language_message', 'Translation'),
            ];
        }

        public function getSources()
        {
            return $this->hasOne(LanguageSource::className(), ['id' => 'id']);
        }
    }
