<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Json;

    /**
     * This is the model class for table "{{%mailer}}".
     *
     * @property integer $id
     * @property integer $mailer_message_id
     * @property integer $mailer_subscriber_id
     * @property string $send_date
     * @property string $status_text
     * @property string $status
     * @property string $send_to
     *
     * @property bool $waiting_for_send
     */
    class Mailer extends ActiveRecord
    {
        public $status_text;
        public $waiting_for_send;

        const STATUS_NOT_SEND = 0;
        const STATUS_SEND = 1;
        const STATUS_CANCELED = 2;

        public static function tableName()
        {
            return '{{%mailer}}';
        }

        public function rules()
        {
            return [
                [['mailer_message_id', 'send_date'], 'required'],
                [['mailer_message_id', 'status'], 'integer'],
                [['send_date'], 'safe'],
                [['send_to'], 'string'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer', 'ID'),
                'mailer_message_id' => Yii::t('mailer', 'Mailer Message ID'),
                'status' => Yii::t('mailer', 'Status'),
                'status_text' => Yii::t('mailer', 'Status'),
                'send_date' => Yii::t('mailer', 'Send Date'),
            ];
        }

        public function afterFind()
        {
            $this->waiting_for_send = false;

            switch($this->status){
                case self::STATUS_NOT_SEND:
                    if($this->send_date <= (new \DateTime())->format("Y-m-d H:i:s")){
                        $this->status_text = Yii::t("mailer", "Not send");
                    }
                    else{
                        $this->status_text = Yii::t("mailer", "Waiting for send");
                        $this->waiting_for_send = true;
                    }
                    break;
                case self::STATUS_SEND:
                    $this->status_text = Yii::t("mailer", "Send");
                    break;
                case self::STATUS_CANCELED:
                    $this->status_text = Yii::t("mailer", "Canceled");
                    break;
            }

            parent::afterFind();
        }

        public function getMessage()
        {
            return $this->hasOne(MailerMessage::className(), ['id' => 'mailer_message_id']);
        }

        public function send()
        {
            switch($this->send_to){
                case "workers":
                    $subscribers = User::find()->where(['status' => User::STATUS_ACTIVE])->all();

                    break;
                case "subscribers":
                    $subscribers = MailerSubscriber::getAll();

                    break;
            }

            if(isset($subscribers) && !empty($subscribers)){
                foreach($subscribers AS $subscriber){
                    $mailData = new MailerData();

                    $mailData->mailer_id = $this->id;

                    if($subscriber instanceof User){
                        $mailData->mailer_user_id = $subscriber->id;
                    }
                    else if($subscriber instanceof MailerSubscriber){
                        $mailData->mailer_subscriber_id = $subscriber->id;
                    }

                    $mailData->save();
                }

                $cronInstance = new CronQueue();

                $cronInstance->data = Json::encode(['id' => $this->id]);
                $cronInstance->description = "Sending mail to clients.";
                $cronInstance->type = "mailer-request";

                return $cronInstance->save();
            }

            return false;
        }
    }
