<?php
    namespace app\models\Settings;

    use app\models\Settings;
    use Yii;

    class System extends Settings
    {
        public $theme;
        public $site_title;
        public $file_manager;
        public $track_level;
        public $currency;
        public $footer_text;
        public $language;
        public $language_level;
        public $navigation_position;

        const MODULE = 'SYSTEM';

        public function rules()
        {
            return [
                [['site_title', 'theme', 'currency', 'footer_text', 'language', 'language_level', 'navigation_position'], 'string'],
                ['file_manager', 'boolean']
            ];
        }

        public function attributeLabels()
        {
            return [
                'site_title' => Yii::t("settings", "Default title"),
                'footer_text' => Yii::t("settings", "Footer text"),
                'file_manager' => Yii::t("settings", "File manager"),
                'language' => Yii::t("settings", "Language"),
                'language_level' => Yii::t("settings", "Language level"),
                'theme' => Yii::t("settings", "Theme"),
                'currency' => Yii::t("settings", "Currency"),
                'navigation_position' => Yii::t("settings", "Navigation position"),
            ];
        }

        public static function getLanguageLevel()
        {
            return [
                'cookie' => Yii::t("settings", "Cookie"),
                'user' => Yii::t("settings", "User"),
                'global' => Yii::t("settings", "Global"),
            ];
        }

        public static function getNavigationPositions()
        {
            return [
                'left' => Yii::t("settings", "Left (sidebar)"),
                'top' => Yii::t("settings", "Top (header)"),
            ];
        }
    }