<?php
    namespace app\models;
    
    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mailer_data}}".
     *
     * @property integer $id
     * @property integer $mailer_id
     * @property integer $mailer_subscriber_id
     * @property integer $mailer_user_id
     *
     * @property Mailer $mailer
     */
    class MailerData extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mailer_data}}';
        }
    
        public function rules()
        {
            return [
                [['mailer_id'], 'required'],
                [['mailer_id', 'mailer_subscriber_id'], 'integer'],
                [['mailer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mailer::className(), 'targetAttribute' => ['mailer_id' => 'id']],
                [['mailer_subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailerSubscriber::className(), 'targetAttribute' => ['mailer_subscriber_id' => 'id']],
                [['mailer_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['mailer_user_id' => 'id']],
            ];
        }
    
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer_data', 'ID'),
                'mailer_id' => Yii::t('mailer_data', 'Mailer ID'),
                'mailer_subscriber_id' => Yii::t('mailer_data', 'Mailer Subscriber ID'),
                'mailer_user-id' => Yii::t('mailer_data', 'Mailer Subscriber ID'),
            ];
        }
    
        public function getMailer()
        {
            return $this->hasOne(Mailer::className(), ['id' => 'mailer_id']);
        }
    }
