<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%cron_queue}}".
     *
     * @property integer $id
     * @property string $data
     * @property string $description
     * @property string $type
     * @property integer $status
     */
    class CronQueue extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%cron_queue}}';
        }

        public function rules()
        {
            return [
                [['description', 'type'], 'required'],
                [['id', 'status'], 'integer'],
                [['data'], 'string'],
                [['description'], 'string', 'max' => 256],
                [['type'], 'string', 'max' => 128],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('cron_queue', 'ID'),
                'data' => Yii::t('cron_queue', 'Data'),
                'status' => Yii::t('cron_queue', 'Status'),
                'description' => Yii::t('cron_queue', 'Description'),
                'type' => Yii::t('cron_queue', 'Type'),
            ];
        }
    }
