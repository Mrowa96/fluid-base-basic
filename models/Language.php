<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%i18n}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $symbol
     */
    class Language extends ActiveRecord
    {
        protected $newLanguage;

        public static function tableName()
        {
            return '{{%i18n}}';
        }

        public function rules()
        {
            return [
                [['name', 'symbol'], 'string'],
                [['name'], 'string', 'max' => 64],
                [['symbol'], 'string', 'max' => 10],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('language', 'ID'),
                'name' => Yii::t('language', 'Name'),
                'symbol' => Yii::t('language', 'Symbol'),
            ];
        }

        public function save($runValidation = true, $attributeNames = null)
        {
            $this->newLanguage = $this->getIsNewRecord();

            return parent::save($runValidation, $attributeNames);
        }
        public function afterSave($insert, $changedAttributes)
        {
            if($this->newLanguage){
                //Add example messages to newly created language
                $sources = LanguageSource::find()->all();

                /** @var LanguageSource $source */
                foreach($sources AS $source){
                    $message = new LanguageMessage();

                    $message->id = $source->id;
                    $message->language = $this->symbol;
                    $message->translation = $source->message;

                    $message->save();
                }

                //Add example mailer message parts for default polish language
                $parts = MailerMessagePart::find()->where(['language' => 'pl_PL'])->all();

                /** @var MailerMessagePart $part */
                foreach($parts AS $part){
                    $newPart = new MailerMessagePart();

                    $newPart->name = $part->name;
                    $newPart->title = $part->title;
                    $newPart->content = $part->content;
                    $newPart->language = $this->symbol;

                    $newPart->save();
                }
            }
            else{
                if(isset($changedAttributes['symbol'])){
                    $messages = LanguageMessage::find()->where(['language' => $changedAttributes['symbol']])->all();

                    /** @var LanguageMessage $message */
                    foreach($messages AS $message){
                        $message->language = $this->symbol;

                        $message->save();
                    }
                }
            }

            parent::afterSave($insert, $changedAttributes);
        }

        public function delete()
        {
            LanguageMessage::deleteAll(['language' => $this->symbol]);
            MailerMessagePart::deleteAll(['language' => $this->symbol]);

            return parent::delete();
        }

        public static function groupByCategories()
        {
            $sources = LanguageSource::find()->all();
            $result = [];

            /** @var LanguageSource $source */
            foreach($sources AS $source){
                $translations = LanguageMessage::find()->where(['id' => $source->id])->all();

                /** @var LanguageMessage $translation */
                foreach($translations AS $translation){
                    $result[$source->category][$source->message][$translation->language] = $translation->translation;
                }
            }

            return $result;
        }
        public static function prepareForManage($id)
        {
            $language = Language::findOne($id);
            $sources = LanguageSource::find()->orderBy('category')->all();
            $data = [];

            /** @var LanguageSource $source */
            foreach($sources AS $source){
                /** @var LanguageMessage $message */
                $message = LanguageMessage::find()->where(['language' => $language->symbol, 'id' => $source->id])->one();

                if($message){
                    $data[$source->category][] = [
                        'id' => $source->id,
                        'source' => $source->message,
                        'message' => $message->translation,
                    ];
                }
                else{
                    $data[$source->category][] = [
                        'id' => $source->id,
                        'source' => $source->message,
                        'message' => $source->message,
                    ];
                }
            }

            return $data;
        }
        public static function prepareForForm()
        {
            return ArrayHelper::map(self::find()->all(), 'symbol', 'name');
        }
        public static function saveMessages()
        {
            $data = Yii::$app->request->post();
            $transaction = LanguageMessage::getDb()->beginTransaction();

            try{
                foreach($data AS $key => $value){
                    if((int) $key){
                        /** @var LanguageMessage $message */
                        $message = LanguageMessage::findOne($key);
                        $message->translation = $value;
                        $message->save();
                    }
                }

                $transaction->commit();

                return true;
            }
            catch(\Exception $ex){
                $transaction->rollBack();
            }

            return false;
        }
    }