<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%mailer_message}}".
     *
     * @property integer $id
     * @property string $title
     * @property string $name
     * @property string $content
     * @property bool $add_header
     * @property bool $add_footer
     * @property bool $system
     * @property bool $language
     *
     * @property string $system_text
     */
    class MailerMessage extends ActiveRecord
    {
        public $system_text;

        public static function tableName()
        {
            return '{{%mailer_message}}';
        }

        public function rules()
        {
            return [
                [['title', 'name', 'content', 'add_header', 'add_footer', 'language'], 'required'],
                [['content'], 'string'],
                [['add_header', 'add_footer'], 'boolean'],
                [['title', 'name'], 'string', 'max' => 256],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer_message', 'ID'),
                'title' => Yii::t('mailer_message', 'Title'),
                'name' => Yii::t('mailer_message', 'Name'),
                'content' => Yii::t('mailer_message', 'Content'),
                'add_header' => Yii::t('mailer_message', 'Include default header'),
                'add_footer' => Yii::t('mailer_message', 'Include default footer'),
                'system' => Yii::t('mailer_message', 'System'),
                'language' => Yii::t('mailer_message', 'Language'),
                'system_text' => Yii::t('mailer_message', 'System text'),
            ];
        }

        public function afterFind()
        {
            $this->system_text = Yii::t("system", ($this->system) ? "Yes" : "No");

            parent::afterFind();
        }

        public function prepareForSent()
        {
            /**
             * @var MailerMessagePart $header
             * @var MailerMessagePart $footer
             */
            if($this->add_header == 1){
                $header = MailerMessagePart::getPart("header", $this->language);

                $this->content = $header->content . $this->content;
            }
            if($this->add_footer == 1){
                $footer = MailerMessagePart::getPart("footer", $this->language);

                $this->content = $this->content . $footer->content;
            }
        }
        public function getTags()
        {
            return $this->hasMany(MailerMessageTag::className(), ['mailer_message_id' => 'id']);
        }
        public function replaceTag($tag, $replacement)
        {
            if(strpos($this->content, '{' . $tag . '}') !== false){
                $this->content = str_replace('{' . $tag . '}', $replacement, $this->content);

                return true;
            }

            return false;
        }
        public function replaceTags(array $data)
        {
            if(!empty($data)){
                foreach($data AS $key => $value){
                    if(!$this->replaceTag($key, $value)){
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(self::find()->where(['system' => 0])->all(), 'id', 'title');
        }
    }
