<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mailer_message_tag}}".
     *
     * @property integer $id
     * @property integer $mailer_message_id
     * @property string $name
     * @property string $description
     *
     * @property MailerMessage $mailMessage
     */
    class MailerMessageTag extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mailer_message_tag}}';
        }

        public function rules()
        {
            return [
                [['mailer_message_id', 'name', 'description'], 'required'],
                [['mailer_message_id'], 'integer'],
                [['description'], 'string'],
                [['name'], 'string', 'max' => 125],
                [['mailer_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailerMessage::className(), 'targetAttribute' => ['mailer_message_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer_message_tag', 'ID'),
                'mailer_message_id' => Yii::t('mailer_message_tag', 'Mailer Message ID'),
                'name' => Yii::t('mailer_message_tag', 'Name'),
                'description' => Yii::t('mailer_message_tag', 'Description'),
            ];
        }

        public function getNewsletterMessage()
        {
            return $this->hasOne(MailerMessage::className(), ['id' => 'mailer_message_id']);
        }
    }
