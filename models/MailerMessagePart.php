<?php
    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mailer_message_part}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $title
     * @property string $content
     * @property string $language
     */
    class MailerMessagePart extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mailer_message_part}}';
        }

        public function rules()
        {
            return [
                [['name', 'title', 'content', 'language'], 'required'],
                [['content'], 'string'],
                [['name'], 'string', 'max' => 128],
                [['title'], 'string', 'max' => 128],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mailer_message_part', 'ID'),
                'name' => Yii::t('mailer_message_part', 'Name'),
                'title' => Yii::t('mailer_message_part', 'Title'),
                'content' => Yii::t('mailer_message_part', 'Content'),
                'language' => Yii::t('mailer_message_part', 'Language'),
            ];
        }

        public static function getPart($name, $language)
        {
            $part = MailerMessagePart::find()->where(['name' => $name, 'language' => $language])->one();

            if(!$part){
                $part = MailerMessagePart::find()->where(['name' => $name])->one();
            }

            return $part;
        }
    }
