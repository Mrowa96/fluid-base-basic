<?php
    namespace app\models;

    use app\models\Settings\System;
    use yii\db\ActiveRecord;
    use yii\helpers\Url;

    /**
     * This is the model class for table "{{%banner}}".
     *
     * @property integer $id
     * @property integer $parent_id
     * @property bool $admin_access
     * @property string $name
     * @property string $icon
     * @property string $symbol
     * @property string $route
     * @property string $route_options
     *
     * @property Menu $parentItem
     * @property string $url
     */
    class Menu extends ActiveRecord{
        public $url;

        public static function tableName(){
            return '{{%menu}}';
        }

        public function getParentItem()
        {
            return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
        }
        public function afterFind(){
            parent::afterFind();

            if($this->symbol === "HOME"){
                $this->url = Url::home();
            }
            else{
                $this->url = Url::toRoute("/" . $this->route);

                if($this->route_options){
                    $this->url .= '?' . $this->route_options;
                }
            }
        }

        public static function fetchForMenu(){
            $groups = Menu::find()->orderBy('position')->all();
            $results = [];

            if(!empty($groups)){
                /** @var Menu $group */
                foreach($groups AS $group){
                    if($group->symbol === "FILEMANAGER"){
                        $fileManagerEnabled = Settings::getOne("file_manager", Settings\System::MODULE);

                        if(!$fileManagerEnabled){
                            continue;
                        }
                    }

                    $results[] = $group;
                }
            }

            return $results;
        }
    }