<?php
    namespace app\forms;

    use Yii;
    use yii\base\Model;
    use app\models\User;

    class Login extends Model
    {
        public $username;
        public $password;
        public $rememberMe = true;
        private $_user = false;

        public function rules()
        {
            return [
                [['username', 'password'], 'required'],
                ['rememberMe', 'boolean'],
                ['password', 'validatePassword'],
            ];
        }

        public function attributeLabels(){
            return [
                'username' => Yii::t("auth", "Username"),
                'password' => Yii::t("auth", "Password"),
            ];
        }

        public function validatePassword($attribute, $params)
        {
            if (!$this->hasErrors()) {
                $user = $this->getUser();

                if (!$user || !$user->validatePassword($this->password, $params)) {
                    $this->addError($attribute, 'Incorrect username or password.');
                }
            }
        }

        public function login()
        {
            if ($this->validate()) {
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
            }
            return false;
        }

        public function getUser()
        {
            if ($this->_user === false) {
                $this->_user = User::findByUsername($this->username);
            }

            return $this->_user;
        }
    }
