<?php
    namespace app\forms;

    use Yii;
    use yii\base\Model;
    use app\models\User;
    use yii\helpers\Url;
    use app\models\Settings;
    use app\utilities\Mailer;
    use app\models\MailerMessage;

    class PasswordResetRequest extends Model
    {
        public $email;

        public function rules()
        {
            return [
                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'exist',
                    'targetClass' => User::className(),
                    'filter' => ['status' => User::STATUS_ACTIVE],
                    'message' => Yii::t("auth", 'There is no user with such email.')
                ],
            ];
        }

        public function sendEmail()
        {
            /**
             * @var User $user
             * @var Mailer $mailer
             * @var MailerMessage $message
             */
            $mailer = Yii::$app->mailer;
            $user = User::findOne(['status' => User::STATUS_ACTIVE, 'email' => $this->email]);
            $message = MailerMessage::find()->where(['name' => 'password_reset_request', 'language' => Yii::$app->language])->one();

            if (!$user || !$message) {
                return false;
            }

            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if (!$user->save()) {
                return false;
            }

            $message->prepareForSent();
            $message->replaceTag("reset_password_url",
                "<a href='" . Yii::$app->request->hostInfo . Url::toRoute(['site/reset-password']) . '?token=' . $user->password_reset_token . "'>Link</a>"
            );

            return $mailer->compose()
                ->setFrom([$mailer->getLogin() => Settings::getOne('site_title', Settings\System::MODULE) . ' robot'])
                ->setTo($this->email)
                ->setSubject($message->title)
                ->setHtmlBody($message->content)
                ->send();
        }
    }
