<?php
    namespace app\forms;

    use Yii;
    use yii\base\Model;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;
    use fluid\fileManager\File;

    class MailerSubscriber extends Model
    {
        public $csvFile;

        public function rules()
        {
            return [
                [['csvFile'], 'file', 'extensions' => 'csv', 'checkExtensionByMimeType' => false]
            ];
        }

        public function attributes()
        {
            return [
                'csvFile' => Yii::t("mail", "CSV File"),
            ];
        }

        public function load($data, $formName = null)
        {
            if(parent::load($data, $formName)){
                $this->csvFile = UploadedFile::getInstance($this, 'csvFile');

                return true;
            }

            return false;
        }

        public function getCsvFile()
        {
            if($this->validate()){
                $filePath = Yii::getAlias("@dataSubscribers") . '/' . $this->csvFile->basename . '.' . $this->csvFile->extension;
                $fileObject = new File($filePath);
                $fileObject->saveUploadedFile(ArrayHelper::toArray($this->csvFile), true);

                return $fileObject;
            }
            else{
                return false;
            }
        }
    }