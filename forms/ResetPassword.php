<?php
    namespace app\forms;

    use Yii;
    use yii\base\Model;
    use yii\base\InvalidParamException;
    use app\models\User;

    class ResetPassword extends Model
    {
        public $password;
        public $password_repeat;

        private $_user;

        public function __construct($token, $config = [])
        {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamException('Password reset token cannot be blank.');
            }

            $this->_user = User::findByPasswordResetToken($token);

            if (!$this->_user) {
                throw new InvalidParamException('Wrong password reset token.');
            }

            parent::__construct($config);
        }

        public function rules()
        {
            return [
                [['password', 'password_repeat'], 'required'],
                [['password', 'password_repeat'], 'string', 'min' => 6],
                ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t("auth", "Passwords do not match")],
            ];
        }

        public function attributeLabels()
        {
            return [
                'password' => Yii::t("auth", "Password"),
                'password_repeat' => Yii::t("auth", "Password repeat"),
            ];
        }

        public function resetPassword()
        {
            /** @var User $user */
            $user = $this->_user;
            $user->setPassword($this->password);
            $user->removePasswordResetToken();

            return $user->save(false);
        }
    }
